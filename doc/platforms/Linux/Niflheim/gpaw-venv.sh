#!/usr/bin/bash
echo "Please use ./gpaw_venv.py instead!  See:

    https://gpaw.readthedocs.io/platforms/Linux/Niflheim/build.html
"
